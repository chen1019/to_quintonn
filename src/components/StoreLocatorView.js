import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Platform, View, Text, StyleSheet, TouchableOpacity, ListView, Alert, Picker} from 'react-native';
import {AppColors, AppConstants, AppStyles, AppSizes, AppErrors} from '../constants';
import Spinner from 'react-native-loading-spinner-overlay';
import {fetchBranches, openTab} from '../actions';
import Icon from 'react-native-vector-icons/FontAwesome';
import axios from 'axios';
import SearchBar from 'react-native-searchbar'
import {Actions} from 'react-native-router-flux';

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class StoreLocatorView extends Component {
	
	constructor(props) {
		super();
		
		let defaultStore = props.branches.length > 0 ? props.branches[0] : null;
		let lat = -33.9249, long = 18.4241;
		if (defaultStore) {
			lat = Number(defaultStore.Coordinates.split(",")[0]);
			long = Number(defaultStore.Coordinates.split(",")[1]);
		}
		
		this.state = {
			dataSource: ds.cloneWithRows(props.branches),
			loading: false,
			branches: props.branches,
			selectedStore: null,
			latitude: lat,
			longitude: long,
			showPicker: false,
		};
  }

  renderRightButton = () => {
    return(
			<TouchableOpacity onPress={() => this.selectRegion() } >
				<Icon name="location-arrow" style={styles.rightButton} />
			</TouchableOpacity>
    );
  };

  componentWillMount() {
    Actions.refresh({ renderRightButton: this.renderRightButton });
  }

  closeModal () {
		this.props.closeModal("storeLocator");
	}
	
	componentDidMount() {
		if (this.state.branches.length === 0)
			this.retrieveBranches();
	}
	
	retrieveBranches() {
		let _this = this;
		const headers = {
			'Authorization': this.props.token
		};
		this.setState({loading: true});
		
		axios.get(
			AppConstants.BASE_URL + 'retrieveBranches',
			{
				headers: headers
			}
		)
			.then(
				function (response) {
					console.log("=================Fetch Branches Success=================");
					_this.setState({
						branches: response.data,
						loading: false,
						dataSource: ds.cloneWithRows(response.data),
						latitude: Number(response.data[0].Coordinates.split(",")[0]),
						longitude: Number(response.data[0].Coordinates.split(",")[1]),
						selectedStore: response.data[0],
					});
					_this.props.fetchBranches(response.data);
				}
			)
			.catch(
				function (error) {
					console.log("=================Fetch Branches FAILED=================");
					_this.setState({loading: false});
          if (error.response.status === AppErrors.AUTH_FAILED) {
            Actions.auth({ redirect: 'storeLocator' });
            return;
          }
          setTimeout(()=> {
						Alert.alert(
							'Fetch Error',
							'Failed to Collect Stores...',
							[
								{text: 'OK', onPress: () => console.log('OK Pressed')},
							],
							{ cancelable: false }
						);
					}, 200);
				}
			);
	}

  onSelectStore(branch) {
    this.props.openTab({
    	name: 'mapView',
			branch,
    });
    // this.refs.storeMapModal.open();
  }

  handleSearchResults(input) {
		if (!input) {
			this.setState({dataSource: ds.cloneWithRows(this.state.branches)});
		} else {
			let branches = [];
			this.props.branches.map((branch) => {
				if (branch.BranchName.toLowerCase().indexOf(input.toLowerCase()) >= 0 ) {
					branches.push(branch);
				}
			});
			
			this.setState({dataSource: ds.cloneWithRows(branches)});
		}
		
	}

  selectRegion() {
    this.setState({ showPicker: !this.state.showPicker });
  }

  handleSelectRegion(region) {
    const filteredBranches = this.props.branches.filter(branch => branch.Region === region);
    this.setState({
    	dataSource: ds.cloneWithRows(filteredBranches),
			regionFilter: region,
      showPicker: false,
		});
	}

	render() {
		const {showPicker, regionFilter} = this.state;

		return (
			<View style={{flex: 1, marginTop: AppSizes.navbarHeight, flexDirection: 'column'}}>
				<Spinner visible={this.state.loading} textStyle={{color: '#FFF'}}/>
        {showPicker &&
				<View>
					<Picker
						selectedValue={regionFilter}
						onValueChange={(itemValue, itemIndex) => this.handleSelectRegion(itemValue)}>
						<Picker.Item key='east' label="East" value="EAST" />
						<Picker.Item key='west' label="West" value="WEST" />
						<Picker.Item key='north' label="North" value="NORTH" />
						<Picker.Item key='south' label="South" value="SOUTH" />
						<Picker.Item key='central' label="Central" value="CENTRAL" />
					</Picker>
				</View>}
				<View>
          {
            this.props.branches.length > 0 ?
							<SearchBar
								ref={(ref) => this.searchBar = ref}
								data={this.props.branches}
								handleSearch={this.handleSearchResults.bind(this)}
								iOSPadding={false}
								showOnLoad
								hideBack
							/>
              : null
          }
				</View>
				<ListView
					style={[styles.container, {marginTop: 53}]}
					dataSource={this.state.dataSource}
					enableEmptySections
					renderRow={
						(branch) => {
							return (
								<TouchableOpacity key={branch.Id}
								                  onPress={ () => this.onSelectStore(branch) }
								>
									<View style={[AppStyles.row, styles.branchInfoContainer]}>
										<View style={styles.branchContainer}>
											<View style={AppStyles.row}>
												<Icon name="book" style={styles.iconBig} />
												<Text style={styles.branchName}> {branch.BranchName} </Text>
											</View>

											<View style={AppStyles.row}>
												<Icon name="phone" style={styles.iconCommon} />
												<Text style={styles.commonText}> {branch.Telephone} </Text>
											</View>

											<View style={AppStyles.row}>
												<Icon name="envelope-o" style={styles.iconCommon} />
												<Text style={styles.commonText}> {branch.Email} </Text>
											</View>

											<View style={AppStyles.row}>
												<Icon name="user" style={styles.iconCommon} />
												<Text style={styles.colorPrimary}> {branch.ManagerName} </Text>
											</View>

											<View style={AppStyles.row}>
												<Icon name="location-arrow" style={styles.iconCommon} />
												<Text style={styles.commonText}> {branch.Region} </Text>
											</View>

											<View style={AppStyles.row}>
												<Icon name="clock-o" style={styles.iconCommon} />
												<Text
													style={[{fontWeight: 'bold'}, branch.Status == "OPEN" ? styles.colorPrimary : styles.colorBase]}>{branch.Status}</Text>
											</View>

											<View style={[AppStyles.row, {paddingHorizontal: 8, paddingVertical: 2}]}>
												<Text style={[styles.colorCommon, styles.weekday]}>Monday</Text>
												<Text style={[styles.colorCommon]}>: </Text>
												<Text style={[styles.colorPrimary, styles.workHour]}>{branch.MondayHours.toUpperCase()}</Text>
											</View>

											<View style={[AppStyles.row, {paddingHorizontal: 8, paddingVertical: 2}]}>
												<Text style={[styles.colorCommon, styles.weekday]}>Tuesday</Text>
												<Text style={[styles.colorCommon ]}>: </Text>
												<Text style={[styles.colorPrimary, styles.workHour]}>{branch.TuesdayHours.toUpperCase()}</Text>
											</View>

											<View style={[AppStyles.row, {paddingHorizontal: 8, paddingVertical: 2}]}>
												<Text style={[styles.colorCommon, styles.weekday]}>Wednesday</Text>
												<Text style={[styles.colorCommon ]}>: </Text>
												<Text style={[styles.colorPrimary, styles.workHour]}>{branch.WednesdayHours.toUpperCase()}</Text>
											</View>

											<View style={[AppStyles.row, {paddingHorizontal: 8, paddingVertical: 2}]}>
												<Text style={[styles.colorCommon, styles.weekday]}>Thursday</Text>
												<Text style={[styles.colorCommon ]}>: </Text>
												<Text style={[styles.colorPrimary, styles.workHour]}>{branch.ThursdayHours.toUpperCase()}</Text>
											</View>

											<View style={[AppStyles.row, {paddingHorizontal: 8, paddingVertical: 2}]}>
												<Text style={[styles.colorCommon, styles.weekday]}>Friday</Text>
												<Text style={[styles.colorCommon ]}>: </Text>
												<Text style={[styles.colorPrimary, styles.workHour]}>{branch.FridayHours.toUpperCase()}</Text>
											</View>

											<View style={[AppStyles.row, {paddingHorizontal: 8, paddingVertical: 2}]}>
												<Text style={[styles.colorCommon, styles.weekday]}>Saturday</Text>
												<Text style={[styles.colorCommon ]}>: </Text>
												<Text style={[styles.colorPrimary, styles.workHour]}>{branch.SaturdayHours.toUpperCase()}</Text>
											</View>

											<View style={[AppStyles.row, {paddingHorizontal: 8, paddingVertical: 2}]}>
												<Text style={[styles.colorCommon, styles.weekday]}>Sunday</Text>
												<Text style={[styles.colorCommon ]}>: </Text>
												<Text style={[styles.colorPrimary, styles.workHour]}>{branch.SundayHours.toUpperCase()}</Text>
											</View>

											<View style={[AppStyles.row, {paddingHorizontal: 8, paddingVertical: 2}]}>
												<Text style={[styles.colorCommon, styles.weekday]}>Public Holiday</Text>
												<Text style={[styles.colorCommon ]}>: </Text>
												<Text style={[styles.colorPrimary, styles.workHour]}>{branch.PublicHolidayHours.toUpperCase()}</Text>
											</View>
										</View>

										<Icon name="angle-right" style={{fontSize: 22, color: AppColors.textPrimary}} />
									</View>
								</TouchableOpacity>
							)
						}
					}
				/>
			</View>
		);
	}
}

const styles = StyleSheet.create({

	navHeader: {
		height: AppSizes.navbarHeight,
		backgroundColor: AppColors.base,
		...Platform.select({
			ios: {
				paddingTop: 22,
			},
			android: {
				paddingTop: 0,
			},
			vr: {
				paddingTop: 0,
			}
		})
	},

	container: {
		flex: 1,
	},
	
	iconBig: {
		width: 18,
		fontSize: 14,
		marginTop: 4,
		color: AppColors.textSecondary,
		textAlign: 'center'
	},
	
	iconCommon: {
		width: 18,
		fontSize: 12,
		marginTop: 3,
		color: AppColors.textSecondary,
		textAlign: 'center'
	},
	
	branchContainer: {
		flex: 1,
	},

	branchInfoContainer: {
		justifyContent: 'center',
		alignItems: 'center',
		padding: 8,
		borderColor: AppColors.listItemBackground,
		borderBottomWidth: 1,
	},

	branchName: {
		fontSize: 16,
		color: AppColors.textPrimary,
	},
	
	commonText: {
		color: AppColors.textSecondary,
	},
	
	colorPrimary: {
		color: AppColors.colorPrimary,
	},
	
	mapView: {
		...StyleSheet.absoluteFillObject
	},

  leftButton: {
    fontSize: 22,
    color: 'white',
    marginLeft: 10,
  },

  rightButton: {
    fontSize: 22,
    color: 'white',
    marginRight: 10,
  },
});

const mapStateToProps = (state) => {
	return {
		token: state.auth.token,
		branches: state.data.branches,
	};
};

export default connect(mapStateToProps, {fetchBranches, openTab})(StoreLocatorView);
